export const ADD_QR = 'ADD_QR';
export const ADD_ALL_QR_CODES = 'ADD_ALL_QR_CODES';

export const addAllQrCodes = (qrCodes) => {
    return{
        type: ADD_ALL_QR_CODES,
        qrCodes
    };
};
export const addQr = (text) => {
    return{
        type: ADD_QR,
        text
    };
};