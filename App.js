import React from 'react';
import {StyleSheet} from 'react-native';

import {combineReducers, createStore} from "redux";
import qrReducer from "./store/qrReducer";
import {Provider} from "react-redux";
import MainScreen from "./screens/MainScreen";

const rootReducer = combineReducers({
    qrCodes: qrReducer
});
const store = createStore(rootReducer);

export default function App() {
    return (
        <Provider store={store}>
            <MainScreen/>
        </Provider>
    );
}